<?php
namespace LivrariaAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;


class CategoriasController extends AbstractActionController
{

    /*
     * @var EntityManager
     */
    protected $em;

    public function indexAction()
    {
        $list = $this->getEm()
                ->getRepository('Livraria\Entity\Categoria')
                ->findAll();
        
        return ViewModel( array('data'=>$list) );
    }
    
    protected function getEm()
    {
        if($this->em === null)
        {
          $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        
      return $this->em;
    }
}
